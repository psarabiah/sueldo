package cl.psarabia.sueldo

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class ContratoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_contrato)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        val sueldoC = findViewById<EditText>(R.id.editSueldoC)
        val sueldoF = findViewById<TextView>(R.id.textSueldoFC)
        val buttonC = findViewById<Button>(R.id.buttonCalcularC)
        buttonC.setOnClickListener {
            val bruto = sueldoC.text.toString().toDoubleOrNull() ?: 0.0
            val sueldoContrato = Contrato(bruto)
            val sueldoLiquido = sueldoContrato.calcularLiquido()
            sueldoF.text = "El Sueldo liquido es: $sueldoLiquido"
        }
        val buttonV = findViewById<Button>(R.id.buttonVC)
        buttonV.setOnClickListener {
            finish()
        }
    }
}