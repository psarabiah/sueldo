package cl.psarabia.sueldo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

class HonorarioActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent { MainHC() }
    }
}
@Preview
@Composable
private fun MainHC(){
    var bruto by remember { mutableStateOf("") }
    var resultado by remember { mutableStateOf("El Sueldo liquido es: 0.0")}
    val contexto = LocalContext.current as ComponentActivity

    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = stringResource(R.string.textHeaderH),
            style = TextStyle(fontSize = 30.sp, fontWeight = FontWeight.Bold)
        )
        Spacer(modifier = Modifier.height(20.dp))
        TextField(
            placeholder = {Text(stringResource(R.string.textSueldo))},
            value = bruto,
            onValueChange = { bruto = it },
            keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Number)
        )
        Spacer(modifier = Modifier.height(20.dp))
        Button(onClick = {

            val sueldoBruto = bruto.toDoubleOrNull() ?: 0.0
            val sueldoHonorario = Honorario(sueldoBruto)
            val sueldoLiquido = sueldoHonorario.calcularLiquido()
            resultado = "El Sueldo liquido es: ${sueldoLiquido}"
        },
            colors = ButtonDefaults.buttonColors(containerColor = Color(0xFFcf3c34))
            ){
            Text(stringResource(R.string.textCalcular))
        }
        Spacer(modifier = Modifier.height(20.dp))
        Text(resultado)
        Spacer(modifier = Modifier.height(20.dp))
        Button(onClick = { contexto.finish() },
            colors = ButtonDefaults.buttonColors(containerColor = Color(0xFFcf3c34))
        ) {
            Text(stringResource(R.string.textVolver))
        }
    }
}